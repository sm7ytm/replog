-- # Fil för skapande av databasen för replog-programmet

-- # Fält att skapa:	
--
--	maskin_id		ett unikt id-nummer som ges vid incheckningen av maskinen, kortet eller liknande
--	felkod			som en specifik felkod finns anges den här, eventuellt kan en tabell läggas till med innebörden av koden
--	maskintyp		tvätt, tumlare eller vad det är för annan maskintyp
--	felbeskrivning	en så utförlig beskrivning som möjligt av hur felet manifesterar sig med all tänkbar information
--	koll_komp		kontrollerade komponenter
--	bytt_komp		utbytta komponenter
--	resultat		resultatet av kollade/utbytta komponenter, en fortsättning av felsökningen kan bli nödvändig om felet kvarstår
--  ticket_id		id-nummer på själva arbetsordern, detta nummer kan refereras till från andra arbetsordrar
--  referencing_id	här anges dom ticket_id som innehåller tidigare reparationer eller mer information om ett specifikt maskin_id


CREATE TABLE reparation ( 
Ticket_ID varchar(6),
Referencing_IDs varchar(60),
Maskin_ID varchar(6), 
Felkod varchar(6), 
Maskintyp varchar(12), 
Felbeskrivning varchar(255), 
Kollade_Komponenter varchar(255), 
Bytade_Komponenter varchar(255), 
Resultat varchar(255) 
);
